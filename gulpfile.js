var gulp = require("gulp");
var browserSync = require("browser-sync").create();
var uglify = require("gulp-uglify");
let cleanCSS = require("gulp-clean-css");
var concat = require("gulp-concat");
const htmlmin = require("gulp-htmlmin");
const { watch } = require("gulp");
var CacheBuster = require("gulp-cachebust");
var cachebust = new CacheBuster();
var sass = require("gulp-sass");

let minifyCssTask = cacheBust => () => {
  let result = gulp
    .src([
      "src/css/bootstrap.min.css",
      "src/css/global-style.css",
      "src/css/bootstrap.css",
      "src/css/style.css",
      "src/css/*.css",
      "!src/css/bundle*.css"
    ])
    .pipe(cleanCSS({ compatibility: "ie8" }))
    .pipe(concat("bundle.css"))
    .pipe(gulp.dest("src/css"));
  if (cacheBust) {
    result.pipe(cachebust.resources()).pipe(gulp.dest("src/css"));
  }
  return result;
};

let minifyJsTask = cacheBust => () => {
  let result = gulp
    .src([
      "src/js/jquery.min.js",
      "src/js/bootstrap*.js",
      "src/js/*.js",
      "!src/js/bundle*.js"
    ])
    .pipe(uglify())
    .pipe(concat("bundle.js"))
    .pipe(gulp.dest("src/js"));

  if (cacheBust) {
    result.pipe(cachebust.resources()).pipe(gulp.dest("src/js"));
  }

  return result;
};

let buildHtml = () =>
  gulp
    .src("src/*.html")
    .pipe(cachebust.references())
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest("src"));

let sassTask = () =>
  gulp
    .src(["src/scss/*.scss"])
    .pipe(sass())
    .pipe(gulp.dest("src/css"));

let serve = () => {
  browserSync.init({
    server: "./src"
  });
  watch(["src/js/*.js", "!src/js/bundle*.js"], minifyJsTask(false));
  watch(["src/css/*.css", "!src/css/bundle*.css"], minifyCssTask(false));
  watch(["src/scss/*.scss"], sassTask);
  gulp.watch("src/**/*.html").on("change", browserSync.reload);
  gulp.watch("src/css/bundle*").on("change", browserSync.reload);
  gulp.watch("src/js/bundle*").on("change", browserSync.reload);
};

gulp.task(
  "build-html",
  gulp.series(minifyCssTask(true), minifyJsTask(true), buildHtml)
);
gulp.task("minify-css", minifyCssTask(false));
gulp.task("minify-js", minifyJsTask(false));
gulp.task(
  "default",
  gulp.series(sassTask, minifyCssTask(false), minifyJsTask(false), serve)
);
gulp.task(
  "build",
  gulp.series(sassTask, minifyCssTask(true), minifyJsTask(true), buildHtml)
);
