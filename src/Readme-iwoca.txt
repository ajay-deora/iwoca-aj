Dear Nick,

Thank you once again for giving me the opportunity to work on the below task for the position 'UI Developer'.
https://www.figma.com/file/uQWx2FOrmzB7AcbpPbmJNk/iwoca-frontend?node-id=0%3A1

It was a great experience working and enjoyed working from the great UI Design.

Within the given timeframe of 4hrs, I tried to finish the two landing pages (Laptop/Desktop & Iphone X views) as close as I could.

Here's what I achieved and tech used (within the 4hrs):
- Bootstrap 4 Grid system
- Sass (css compiled) 
- Bitbucket / SourceTree for version control
- The landing pages is optimised for desktop / laptop & phones (iphone X, 6, SE and other phones)
- As you use compiled / custom fonts, I used another accessible font

If I had more time:

- I would have set up a theme to avoid CSS repetition
- Would have had a different approach when content are not the same on desktop screen vs mobile screen
- I would refined the mobile landscape and tablet view
- I would have spent more time refining the aesthetic (pixel perfect from UI Design)
- Better code structure

Source code can be downloaded here:
https://bitbucket.org/ajay-deora/iwoca-aj/src/master/

Code deployed online here:
www.ajaydeora.com/iwoca  [Note: This website is not Https]

I hope you liked the work so far and looking forward to discussing further and take you though the journey.

Many thanks.
Ajay Deora